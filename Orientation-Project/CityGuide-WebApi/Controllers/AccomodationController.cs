﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using CityGuide_WebApi.Data;
using CityGuide_WebApi.Models;
using CityGuide_WebApi.View_Layer;
using Microsoft.AspNetCore.Mvc;

namespace CityGuide_WebApi.Controllers
{


    [Route("api/[controller]")]
    [ApiController]
    public class AccomodationController : ControllerBase
    {
        private readonly IMapper _mapper;
        private readonly ApplicationContext _context;

        public AccomodationController(IMapper mapper, ApplicationContext context)
        {
            _mapper = mapper;
            _context = context;
        }


        [HttpGet]
        [Route("GetAccommodations")]
        public ActionResult GetAccommodations()
        {
            try
            {
                BaseTable baseAccommodations = (BaseTable)_context.BaseTable.Where(item => item.CategoryId == 4);
                if(baseAccommodations != null)
                {
                    Guid entityId = baseAccommodations.ID;
                    
                    AccommodationAmenities accommodationAmenities = _context.AccommodationAmenities.FirstOrDefault(item => item.Id == entityId);

                    if (accommodationAmenities != null)
                    {
                        AddAccommodationAmenitiesViewModel addAccommodationAmenitiesViewModel = new AddAccommodationAmenitiesViewModel();

                        addAccommodationAmenitiesViewModel = _mapper.Map<AddAccommodationAmenitiesViewModel>(baseAccommodations);
                        addAccommodationAmenitiesViewModel = _mapper.Map<AddAccommodationAmenitiesViewModel>(accommodationAmenities);
                            
                        var entityImages = _context.EnitityImages.Where(item => item.EntityID == entityId).ToList();
                        
                        if(entityImages != null)
                        {
                            Dictionary<string, Object> EntryData = new Dictionary<string, object>();
                            EntryData.Add("EntityData", addAccommodationAmenitiesViewModel);
                            EntryData.Add("Images", entityImages);

                            return Ok(EntryData);
                        }

                        return BadRequest("No data exists for request");

                    }

                    return BadRequest("No amenities exists for request");

                }

                return BadRequest("No data exists for request");

            }
            catch (Exception)
            {
                return BadRequest();
            }
           
           

           

            

            

        }


        // POST: api/Accomodation
        [HttpPost]
        [Route("AddAccommodation")]
        public ActionResult Post([FromBody] AddAccommodationAmenitiesViewModel addAccommodation )
        {
            try{ 
                
                string Name =_context.BaseTable.FirstOrDefault(item => item.Name == addAccommodation.Name).ToString();
                
                if(Name == null)
                {
                    BaseTable baseEntry = _mapper.Map<BaseTable>(addAccommodation);
                    AccommodationAmenities accommodationAmenities = _mapper.Map<AccommodationAmenities>(addAccommodation);
                    
                    baseEntry.CategoryId = 4;
                    
                    var result = _context.BaseTable.Add(baseEntry);
                    _context.SaveChanges();
                    
                    BaseTable getData = result.Entity;
                    Guid Id = getData.ID;
                    accommodationAmenities.Id = Id;
                    
                    _context.AccommodationAmenities.Add(accommodationAmenities);
                    _context.SaveChanges();
                }
                else
                {
                    return BadRequest("Failed to Add Duplicate Data");
                }


            }
            catch (Exception )
            {
                return BadRequest("Failed to Add Accomodation Data");
            }
            return Ok("Successfully Posted");
        }

        // PUT: api/Accomodation/5
        [HttpPut()]
        public void Put()
        {

        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public void Delete(Guid id)
        {
            AccommodationAmenities accommodationAmenities = _context.AccommodationAmenities.Find(id);
            if(accommodationAmenities != null)
            {
                _context.AccommodationAmenities.Remove(accommodationAmenities);

                EntityImages entityImages = _context.EnitityImages.Find(id);
                if( entityImages != null)
                {
                    _context.EnitityImages.Remove(entityImages);
               
                    BaseTable baseData =_context.BaseTable.Find(id);
                
                    if(baseData != null)
                    {
                       _context.BaseTable.Remove(baseData);
                    
                    }
                }    
            }
        }
    }
}
