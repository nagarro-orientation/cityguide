﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using CityGuide_WebApi.Data;
using CityGuide_WebApi.Models;

using CityGuide_WebApi.View_Layer;

using Microsoft.AspNetCore.Mvc;

namespace CityGuide_WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ActivitiesController : ControllerBase
    {

        private readonly IMapper _mapper;
        private readonly ApplicationContext _context;

        public ActivitiesController(IMapper mapper, ApplicationContext context)
        {
            _mapper = mapper;
            _context = context;
        }

        // GET: api/Activities
        [HttpGet]
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET: api/Activities/5
        [HttpGet("{id}")]
        public string Get(int id)
        {
            return "value";
        }





        [HttpGet]
        [Route("GetActivities")]
        public ActionResult GetActivities()
        {
            try
            {
                BaseTable baseActivities = (BaseTable)_context.BaseTable.Where(item => item.CategoryId == 2);
                if (baseActivities != null)
                {
                    Guid entityId = baseActivities.ID;

                    ActivitiesAmenities activitiesAmenities = _context.ActivitiesAmenities.FirstOrDefault(item => item.Id == entityId);

                    if (activitiesAmenities != null)
                    {
                        AddActivitiesAmenitiesViewModel ActivitiesAmenitiesViewModelData = new AddActivitiesAmenitiesViewModel();

                        ActivitiesAmenitiesViewModelData = _mapper.Map<AddActivitiesAmenitiesViewModel>(activitiesAmenities);

                        var entityImages = _context.EnitityImages.Where(item => item.EntityID == entityId).ToList();

                        if (entityImages != null)
                        {
                            Dictionary<string, Object> EntryData = new Dictionary<string, object>();
                            EntryData.Add("EntityData", ActivitiesAmenitiesViewModelData);
                            EntryData.Add("Images", entityImages);

                            return Ok(EntryData);
                        }

                        return BadRequest("No data exists for request");

                    }

                    return BadRequest("No amenities exists for request");

                }

                return BadRequest("No data exists for request");

            }
            catch (Exception)
            {
                return BadRequest();
            }


        }


        // POST: api/Activities
        [HttpPost]
        [Route("Activities")]
        public ActionResult Post([FromBody] AddActivitiesAmenitiesViewModel addActivities)
        {
            try
            {
                string name = _context.BaseTable.FirstOrDefault(item => item.Name == addActivities.Name).ToString();

                if (name == null)
                {
                    BaseTable baseEntry = _mapper.Map<BaseTable>(addActivities);
                    ActivitiesAmenities activitiesAmenities = _mapper.Map<ActivitiesAmenities>(addActivities);
                    baseEntry.CategoryId = 2;
                    var result = _context.BaseTable.Add(baseEntry);
                    _context.SaveChanges();

                    var getData = result.Entity;
                    Guid Id = getData.ID;
                    activitiesAmenities.Id = Id;
                    _context.ActivitiesAmenities.Add(activitiesAmenities);
                    _context.SaveChanges();
                }
                else
                {
                    return BadRequest("Failed to Add Duplicate Data");
                }
            } 
            catch (Exception )
            {
                return BadRequest("Failed to Add Accomodation Data");
            }
            return Ok("Successfully Posted");
}


        // PUT: api/Activities/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {

            ActivitiesAmenities activitiesAmenities = _context.ActivitiesAmenities.Find(id);
            if (activitiesAmenities != null)
            {
                _context.ActivitiesAmenities.Remove(activitiesAmenities);

                EntityImages entityImages = _context.EnitityImages.Find(id);
                if (entityImages != null)
                {
                    _context.EnitityImages.Remove(entityImages);

                    BaseTable baseData = _context.BaseTable.Find(id);

                    if (baseData != null)
                    {
                        _context.BaseTable.Remove(baseData);

                    }
                }
            }
        }
    }
}
