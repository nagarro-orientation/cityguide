﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using CityGuide_WebApi.Data;
using CityGuide_WebApi.Models;
using CityGuide_WebApi.View_Layer;
using Microsoft.AspNetCore.Mvc;

namespace CityGuide_WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class FoodController : ControllerBase
    {

        private readonly IMapper _mapper;
        private readonly ApplicationContext _context;

        public FoodController(IMapper mapper, ApplicationContext context)
        {
            _mapper = mapper;
            _context = context;
        }
        
        // GET: api/Food
        [HttpGet]
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET: api/Food/5
        [HttpGet("{id}")]
        public string Get(int id)
        {
            return "value";
        }





        [HttpGet]
        [Route("GetFood")]
        public ActionResult GetFood()
        {
            try
            {
                BaseTable baseActivities = (BaseTable)_context.BaseTable.Where(item => item.CategoryId == 3);
                if (baseActivities != null)
                {
                    Guid entityId = baseActivities.ID;

                    FoodAmenities foodAmenitiesData = _context.FoodAmenities.FirstOrDefault(item => item.Id == entityId);

                    if (foodAmenitiesData != null)
                    {
                        AddFoodAmenitiesViewModel addFoodAmenitiesViewModel = new AddFoodAmenitiesViewModel();

                        addFoodAmenitiesViewModel = _mapper.Map<AddFoodAmenitiesViewModel>(addFoodAmenitiesViewModel);

                        var entityImages = _context.EnitityImages.Where(item => item.EntityID == entityId).ToList();

                        if (entityImages != null)
                        {
                            Dictionary<string, Object> EntryData = new Dictionary<string, object>();
                            EntryData.Add("EntityData", addFoodAmenitiesViewModel);
                            EntryData.Add("Images", entityImages);

                            return Ok(EntryData);
                        }

                        return BadRequest("No data exists for request");

                    }

                    return BadRequest("No amenities exists for request");

                }

                return BadRequest("No data exists for request");

            }
            catch (Exception)
            {
                return BadRequest();
            }


        }
        // POST: api/Food
        [HttpPost]
        [Route("Food")]
        public ActionResult Post([FromBody]  AddFoodAmenitiesViewModel addFood)
        {
            try
            {
                string Name = _context.BaseTable.FirstOrDefault(item => item.Name == addFood.Name).ToString();

                if (Name == null)
                {
                    BaseTable baseEntry = _mapper.Map<BaseTable>(addFood);
                    FoodAmenities foodAmenities = _mapper.Map<FoodAmenities>(addFood);
                    baseEntry.CategoryId = 3;
                    var result = _context.BaseTable.Add(baseEntry);
                    _context.SaveChanges();

                    var getData = result.Entity;
                    Guid Id = getData.ID;
                    foodAmenities.Id = Id;
                    _context.FoodAmenities.Add(foodAmenities);
                    _context.SaveChanges();
                }
                else
                {
                    return BadRequest("Failed to Add Duplicate Data");
                }
            }
            catch (Exception )
            {
                return BadRequest("Failed to Add Accomodation Data");
            }
            return Ok("Successfully Posted");
        }

        // PUT: api/Food/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {

            FoodAmenities foodAmenities = _context.FoodAmenities.Find(id);
            if (foodAmenities != null)
            {
                _context.FoodAmenities.Remove(foodAmenities);

                EntityImages entityImages = _context.EnitityImages.Find(id);
                if (entityImages != null)
                {
                    _context.EnitityImages.Remove(entityImages);

                    BaseTable baseData = _context.BaseTable.Find(id);

                    if (baseData != null)
                    {
                        _context.BaseTable.Remove(baseData);

                    }
                }
            }
        }
    }
}
