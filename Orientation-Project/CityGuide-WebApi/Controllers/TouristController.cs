﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoMapper;
using CityGuide_WebApi.Data;
using CityGuide_WebApi.Models;
using CityGuide_WebApi.View_Layer;
using Microsoft.AspNetCore.Mvc;

namespace CityGuide_WebApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class TouristController : ControllerBase
    {

        private readonly IMapper _mapper;
        private readonly ApplicationContext _context;

        public TouristController(IMapper mapper,ApplicationContext context)
        {
            _mapper = mapper;
            _context = context;
        }

        // GET: api/Tourist
        [HttpGet]
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

      
        // GET: api/Tourist/5
        [HttpGet("{id}")]
        public string Get(int id)
        {
            return "value";
        }


        [HttpGet]
        [Route("GetTourist")]
        public ActionResult GetTourist()
        {
            try
            {
                BaseTable baseActivities = (BaseTable)_context.BaseTable.Where(item => item.CategoryId == 2);
                if (baseActivities != null)
                {
                    Guid entityId = baseActivities.ID;

                    TouristsAmenities touristsAmenities = _context.TouristsAmenities.FirstOrDefault(item => item.Id == entityId);

                    if (touristsAmenities != null)
                    {
                        AddTouristsEntryViewModel addTouristsEntryViewModel = new AddTouristsEntryViewModel();

                        addTouristsEntryViewModel = _mapper.Map<AddTouristsEntryViewModel>(touristsAmenities);

                        var entityImages = _context.EnitityImages.Where(item => item.EntityID == entityId).ToList();

                        if (entityImages != null)
                        {
                            Dictionary<string, Object> EntryData = new Dictionary<string, object>();
                            EntryData.Add("EntityData", addTouristsEntryViewModel);
                            EntryData.Add("Images", entityImages);

                            return Ok(EntryData);
                        }

                        return BadRequest("No data exists for request");

                    }

                    return BadRequest("No amenities exists for request");

                }

                return BadRequest("No data exists for request");

            }
            catch (Exception)
            {
                return BadRequest();
            }


        }

        // POST: api/Tourist 
        [HttpPost]
        [Route("Tourist")]
        public ActionResult Post([FromBody] AddTouristsEntryViewModel addTouristEntry)
        {
            try
            {
                string Name = _context.BaseTable.FirstOrDefault(item => item.Name == addTouristEntry.Name).ToString();

                if (Name == null)
                {
                    BaseTable baseEntry = _mapper.Map<BaseTable>(addTouristEntry);
                    TouristsAmenities touristEntry = _mapper.Map<TouristsAmenities>(addTouristEntry);
                    baseEntry.CategoryId = 1;
                    var result =_context.BaseTable.Add(baseEntry);
                    _context.SaveChanges();

                    var getData =result.Entity;
                    Guid Id = getData.ID;
                    touristEntry.Id = Id;
                    _context.TouristsAmenities.Add(touristEntry);
                    _context.SaveChanges();


                }
                else
                {
                    return BadRequest("Failed to Add Duplicate Data");
                }
            }
            catch (Exception )
            {
                return BadRequest("Failed to Add Accomodation Data");
            }
            return Ok("Successfully Posted");

        }


       /* [HttpPut("{id}")]
        [Route("UpdateTouristEntities")]
        public ActionResult UpdateTouristEntities([FromBody] AddTouristsEntryViewModel updateToutrist)
        {
            if (ModelState.IsValid)
            {

            }
        }*/

      /*  [HttpDelete("{id}")]
        [Route("Deleteitems")]
        public IHttpActionResult Delete(int id)
        {
            if (id <= 0)
            {
                return BadRequest("Not a valid id");
            }
        }*/

        // PUT: api/Tourist/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        // DELETE: api/ApiWithActions/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {

            TouristsAmenities touristsAmenities = _context.TouristsAmenities.Find(id);
            if (touristsAmenities != null)
            {
                _context.TouristsAmenities.Remove(touristsAmenities);

                EntityImages entityImages = _context.EnitityImages.Find(id);
                if (entityImages != null)
                {
                    _context.EnitityImages.Remove(entityImages);

                    BaseTable baseData = _context.BaseTable.Find(id);

                    if (baseData != null)
                    {
                        _context.BaseTable.Remove(baseData);

                    }
                }
            }
        }
    }
}
